#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Upsat Fsk Transmitter
# Generated: Thu Dec  1 16:51:50 2016
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.filter import pfb
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import math
import numpy
import osmosdr
import satnogs
import sys
import time


class upsat_fsk_transmitter(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Upsat Fsk Transmitter")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Upsat Fsk Transmitter")
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "upsat_fsk_transmitter")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Variables
        ##################################################
        self.samples_per_symbol_tx = samples_per_symbol_tx = 4*8
        self.sq_wave = sq_wave = (1.0, ) * samples_per_symbol_tx
        self.gaussian_taps = gaussian_taps = filter.firdes.gaussian(1.0, samples_per_symbol_tx, 1.0, 4*samples_per_symbol_tx)
        self.deviation = deviation = 3.9973e3
        self.baud_rate = baud_rate = 1200
        self.tx_frequency = tx_frequency = 145.835e6
        self.samp_rate_tx = samp_rate_tx = 2e6
        self.rf_gain_tx = rf_gain_tx = 0
        self.rf_gain_rx = rf_gain_rx = 0
        self.modulation_index = modulation_index = deviation / (baud_rate / 2.0)
        self.lo_offset = lo_offset = 100e3
        self.interp_taps = interp_taps = numpy.convolve(numpy.array(gaussian_taps), numpy.array(sq_wave))
        self.if_gain_tx = if_gain_tx = 0
        self.if_gain_rx = if_gain_rx = 0
        self.bb_gain_tx = bb_gain_tx = 0
        self.bb_gain_rx = bb_gain_rx = 0

        ##################################################
        # Blocks
        ##################################################
        self._rf_gain_tx_range = Range(0, 70, 0.5, 0, 200)
        self._rf_gain_tx_win = RangeWidget(self._rf_gain_tx_range, self.set_rf_gain_tx, 'RF Gain TX', "counter_slider", float)
        self.top_layout.addWidget(self._rf_gain_tx_win)
        self._if_gain_tx_range = Range(0, 40, 0.5, 0, 200)
        self._if_gain_tx_win = RangeWidget(self._if_gain_tx_range, self.set_if_gain_tx, 'IF Gain TX', "counter_slider", float)
        self.top_layout.addWidget(self._if_gain_tx_win)
        self._bb_gain_tx_range = Range(0, 40, 0.5, 0, 200)
        self._bb_gain_tx_win = RangeWidget(self._bb_gain_tx_range, self.set_bb_gain_tx, 'BB Gain TX', "counter_slider", float)
        self.top_layout.addWidget(self._bb_gain_tx_win)
        self.satnogs_upsat_fsk_frame_encoder_0 = satnogs.upsat_fsk_frame_encoder([0x33]*8 , [0x7A, 0x0E], False, False, False, True, True, 'ABCD', 0, 'UPSAT', 0, 512)
        self.satnogs_debug_msg_source_0 = satnogs.debug_msg_source('HELLO UPSAT FROM EARTH' * 2, 1, True)
        self._rf_gain_rx_range = Range(0, 40, 0.5, 0, 200)
        self._rf_gain_rx_win = RangeWidget(self._rf_gain_rx_range, self.set_rf_gain_rx, 'RF Gain RX', "counter_slider", float)
        self.top_layout.addWidget(self._rf_gain_rx_win)
        self.pfb_arb_resampler_xxx_0 = pfb.arb_resampler_ccf(
        	  samp_rate_tx / (baud_rate * samples_per_symbol_tx),
                  taps=(firdes.low_pass_2(32, 32, 0.8, 0.1, 60)),
        	  flt_size=32)
        self.pfb_arb_resampler_xxx_0.declare_sample_delay(0)
        	
        self.osmosdr_sink_0 = osmosdr.sink( args="numchan=" + str(1) + " " + '' )
        self.osmosdr_sink_0.set_sample_rate(samp_rate_tx)
        self.osmosdr_sink_0.set_center_freq(tx_frequency - lo_offset, 0)
        self.osmosdr_sink_0.set_freq_corr(0, 0)
        self.osmosdr_sink_0.set_gain(rf_gain_tx, 0)
        self.osmosdr_sink_0.set_if_gain(if_gain_tx, 0)
        self.osmosdr_sink_0.set_bb_gain(bb_gain_tx, 0)
        self.osmosdr_sink_0.set_antenna('', 0)
        self.osmosdr_sink_0.set_bandwidth(samp_rate_tx, 0)
          
        self.interp_fir_filter_xxx_0 = filter.interp_fir_filter_fff(samples_per_symbol_tx, (interp_taps))
        self.interp_fir_filter_xxx_0.declare_sample_delay(0)
        self._if_gain_rx_range = Range(0, 40, 0.5, 0, 200)
        self._if_gain_rx_win = RangeWidget(self._if_gain_rx_range, self.set_if_gain_rx, 'IF Gain RX', "counter_slider", float)
        self.top_layout.addWidget(self._if_gain_rx_win)
        self.blocks_multiply_xx_0 = blocks.multiply_vcc(1)
        self._bb_gain_rx_range = Range(0, 40, 0.5, 0, 200)
        self._bb_gain_rx_win = RangeWidget(self._bb_gain_rx_range, self.set_bb_gain_rx, 'BB Gain RX', "counter_slider", float)
        self.top_layout.addWidget(self._bb_gain_rx_win)
        self.analog_sig_source_x_0 = analog.sig_source_c(samp_rate_tx, analog.GR_COS_WAVE, lo_offset , 1, 0)
        self.analog_frequency_modulator_fc_0 = analog.frequency_modulator_fc((math.pi*modulation_index) / samples_per_symbol_tx)

        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.satnogs_debug_msg_source_0, 'msg'), (self.satnogs_upsat_fsk_frame_encoder_0, 'pdu'))    
        self.connect((self.analog_frequency_modulator_fc_0, 0), (self.pfb_arb_resampler_xxx_0, 0))    
        self.connect((self.analog_sig_source_x_0, 0), (self.blocks_multiply_xx_0, 1))    
        self.connect((self.blocks_multiply_xx_0, 0), (self.osmosdr_sink_0, 0))    
        self.connect((self.interp_fir_filter_xxx_0, 0), (self.analog_frequency_modulator_fc_0, 0))    
        self.connect((self.pfb_arb_resampler_xxx_0, 0), (self.blocks_multiply_xx_0, 0))    
        self.connect((self.satnogs_upsat_fsk_frame_encoder_0, 0), (self.interp_fir_filter_xxx_0, 0))    

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "upsat_fsk_transmitter")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_samples_per_symbol_tx(self):
        return self.samples_per_symbol_tx

    def set_samples_per_symbol_tx(self, samples_per_symbol_tx):
        self.samples_per_symbol_tx = samples_per_symbol_tx
        self.set_sq_wave((1.0, ) * self.samples_per_symbol_tx)
        self.pfb_arb_resampler_xxx_0.set_rate(self.samp_rate_tx / (self.baud_rate * self.samples_per_symbol_tx))
        self.set_gaussian_taps(filter.firdes.gaussian(1.0, self.samples_per_symbol_tx, 1.0, 4*self.samples_per_symbol_tx))
        self.analog_frequency_modulator_fc_0.set_sensitivity((math.pi*self.modulation_index) / self.samples_per_symbol_tx)

    def get_sq_wave(self):
        return self.sq_wave

    def set_sq_wave(self, sq_wave):
        self.sq_wave = sq_wave
        self.set_interp_taps(numpy.convolve(numpy.array(self.gaussian_taps), numpy.array(self.sq_wave)))

    def get_gaussian_taps(self):
        return self.gaussian_taps

    def set_gaussian_taps(self, gaussian_taps):
        self.gaussian_taps = gaussian_taps
        self.set_interp_taps(numpy.convolve(numpy.array(self.gaussian_taps), numpy.array(self.sq_wave)))

    def get_deviation(self):
        return self.deviation

    def set_deviation(self, deviation):
        self.deviation = deviation
        self.set_modulation_index(self.deviation / (self.baud_rate / 2.0))

    def get_baud_rate(self):
        return self.baud_rate

    def set_baud_rate(self, baud_rate):
        self.baud_rate = baud_rate
        self.set_modulation_index(self.deviation / (self.baud_rate / 2.0))
        self.pfb_arb_resampler_xxx_0.set_rate(self.samp_rate_tx / (self.baud_rate * self.samples_per_symbol_tx))

    def get_tx_frequency(self):
        return self.tx_frequency

    def set_tx_frequency(self, tx_frequency):
        self.tx_frequency = tx_frequency
        self.osmosdr_sink_0.set_center_freq(self.tx_frequency - self.lo_offset, 0)

    def get_samp_rate_tx(self):
        return self.samp_rate_tx

    def set_samp_rate_tx(self, samp_rate_tx):
        self.samp_rate_tx = samp_rate_tx
        self.pfb_arb_resampler_xxx_0.set_rate(self.samp_rate_tx / (self.baud_rate * self.samples_per_symbol_tx))
        self.osmosdr_sink_0.set_sample_rate(self.samp_rate_tx)
        self.osmosdr_sink_0.set_bandwidth(self.samp_rate_tx, 0)
        self.analog_sig_source_x_0.set_sampling_freq(self.samp_rate_tx)

    def get_rf_gain_tx(self):
        return self.rf_gain_tx

    def set_rf_gain_tx(self, rf_gain_tx):
        self.rf_gain_tx = rf_gain_tx
        self.osmosdr_sink_0.set_gain(self.rf_gain_tx, 0)

    def get_rf_gain_rx(self):
        return self.rf_gain_rx

    def set_rf_gain_rx(self, rf_gain_rx):
        self.rf_gain_rx = rf_gain_rx

    def get_modulation_index(self):
        return self.modulation_index

    def set_modulation_index(self, modulation_index):
        self.modulation_index = modulation_index
        self.analog_frequency_modulator_fc_0.set_sensitivity((math.pi*self.modulation_index) / self.samples_per_symbol_tx)

    def get_lo_offset(self):
        return self.lo_offset

    def set_lo_offset(self, lo_offset):
        self.lo_offset = lo_offset
        self.osmosdr_sink_0.set_center_freq(self.tx_frequency - self.lo_offset, 0)
        self.analog_sig_source_x_0.set_frequency(self.lo_offset )

    def get_interp_taps(self):
        return self.interp_taps

    def set_interp_taps(self, interp_taps):
        self.interp_taps = interp_taps
        self.interp_fir_filter_xxx_0.set_taps((self.interp_taps))

    def get_if_gain_tx(self):
        return self.if_gain_tx

    def set_if_gain_tx(self, if_gain_tx):
        self.if_gain_tx = if_gain_tx
        self.osmosdr_sink_0.set_if_gain(self.if_gain_tx, 0)

    def get_if_gain_rx(self):
        return self.if_gain_rx

    def set_if_gain_rx(self, if_gain_rx):
        self.if_gain_rx = if_gain_rx

    def get_bb_gain_tx(self):
        return self.bb_gain_tx

    def set_bb_gain_tx(self, bb_gain_tx):
        self.bb_gain_tx = bb_gain_tx
        self.osmosdr_sink_0.set_bb_gain(self.bb_gain_tx, 0)

    def get_bb_gain_rx(self):
        return self.bb_gain_rx

    def set_bb_gain_rx(self, bb_gain_rx):
        self.bb_gain_rx = bb_gain_rx


def main(top_block_cls=upsat_fsk_transmitter, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
